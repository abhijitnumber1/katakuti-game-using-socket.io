const mysql = require('mysql')

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'fems@fusion12#$',
    database : 'katakuti'
});
connection.connect((error)=>{
    if(error)
    {
        console.log('Error Found', error.message);
    }
    else
    {
        console.log('Connected');
    }
});

module.exports = connection;