const db_connection = require('./db_connect')
const express = require('express')
const session = require('express-session')
const bodyParser = require('body-parser')
const app = express()
const io = require('socket.io')();
const port = 3111
var jsonResponse;

//create session
app.use(session({
    secret: 'ssshhhhh',
    resave: false,
    saveUninitialized: false
    // cookie: { secure: true }
}));

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

//create server
app.use(express.static('public'));
var server = app.listen(port, (res,err)=>{
    console.log('Node Server Opend',err);
});
//attach server with socket.io
io.attach(server);

io.on('connection', function(socket){
    socket.on('login', function(msg){
        console.log(msg);
        socket.user_email = msg[0].value;
        socket.user_id = msg[1].value;
        io.emit('addUser', msg);
    });
    socket.on('disconnect',function()
    {
        db_connection.query('UPDATE users SET users.is_available="0" WHERE users.user_id = "'+socket.user_id+'"',function(error,results,fields)
        {
            if(error)
            {
                throw error.message
            }
            else
            {
                console.log(socket.user_id+" User Deactivated");
                io.emit('deactivateUser', socket.user_id);
            }
        });
    });
    socket.on('reconnect',function()
    {
        console.log('reconnect fired');
    })
    console.log('A user is connected ');
});

app.get('/loginstat',(req,res)=>{
    if(req.session && req.session.user_email)
    {
        jsonResponse = {'status':true};
        res.send(jsonResponse);
    }
    else
    {
        jsonResponse = {'status':false};
        res.send(jsonResponse);
    }
});
app.get('/activeusers',(req,res)=>{
    db_connection.query('SELECT * FROM users',function(error,results,fields)
    {
        if(error)
        {
            throw error.message
        }
        else
        {
            res.send(results);
        }
    });
});
var checkExistingUser = (req,res,next)=>
{
    db_connection.query('SELECT * FROM users WHERE users.user_email = "'+req.body.email+'"',function(error,results,fields)
    {
        if(error)
        {
            throw error.message
        }
        else
        {
            req.alreadyLogin = false;
            if(results.length > 0 && results[0].is_available == "0")
            {
                db_connection.query('UPDATE users SET is_available="1" WHERE user_id="'+results[0].user_id+'"',function(error,result,fields)
                {
                    if(error)
                    {
                        throw error.message
                    }
                    else
                    {
                        req.user_id = results[0].user_id
                        next();
                    }
                });
            }
            else if(results.length > 0 && results[0].is_available == "1")
            {
                req.alreadyLogin = true;
                next();
            }
            else
            {
                db_connection.query('INSERT INTO users(user_email) VALUES("'+req.body.email+'")',function(error,results,fields)
                {
                    if(error)
                    {
                        throw error.message
                    }
                    else
                    {
                        req.user_id = results.insertId
                        next();
                    }
                });
            }
        }
    });
}
app.post('/login',checkExistingUser,(req,res,next)=>{
    if(req.body.email != '' && req.alreadyLogin == false)
    {
        req.session.user_email = req.body.email;
        jsonResponse = {'status':true,'user_id':req.user_id};
        res.send(jsonResponse);
    }
    else
    {
        jsonResponse = {'status':false};
        res.send(jsonResponse);
    }
});